#define MAINPREFIX x
#define PREFIX zeus_keybinds

#include "script_version.hpp"

#define VERSION     MAJOR.MINOR
#define VERSION_STR MAJOR.MINOR.PATCHLVL.BUILD
#define VERSION_AR  MAJOR,MINOR,PATCHLVL,BUILD

#define REQUIRED_VERSION 2.18

#define COMPONENT_NAME QUOTE(Zeus Keybinds)