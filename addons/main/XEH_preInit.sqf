#include "script_component.hpp"

ADDON = false;

PREP_RECOMPILE_START;
#include "XEH_PREP.hpp"
PREP_RECOMPILE_END;

// Reset everytime as value is very mission specific
GVAR(effectValue) = EFFECT_STEPS select round(((count EFFECT_STEPS) - 1) / 2);
GVAR(ZeusTPIndex) = 0;
GVAR(ZeusTPLastUsed) = 0;

GVAR(aceArsenalLoaded) = isClass (configFile >> "CfgPatches" >> "ace_arsenal");
GVAR(lambsWPLoaded) = isClass (configFile >> "CfgPatches" >> "lambs_wp");

#include "initSettings.inc.sqf"

// Always load
#include "initKeybinds.inc.sqf"
#include "initKeybindsAI.inc.sqf"
#include "initKeybindsEditor.inc.sqf"

// Check optional supported mods
if (GVAR(aceArsenalLoaded)) then
{
	#include "initKeybindsACE.inc.sqf"
};

ADDON = true;