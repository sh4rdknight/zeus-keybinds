// Optional mods
PREP(ACE_switchCaptiveStatus);


PREP(addKeybindUI);
PREP(canUseKeybind);
PREP(clearInventory);
PREP(cycleDoorState);
PREP(followTarget);
PREP(forceDelete);
PREP(garrisonUnits);
PREP(getAmmo);
PREP(getBuildingsWithDoors);
PREP(getClosestPosition);
PREP(getSelection);
PREP(getUnitAmmo);
PREP(getVehicleAmmo);
PREP(handleUnitsSideChanged);
PREP(healObjects);
PREP(removeWeapons);
PREP(safeDelete);
PREP(spawnArsenal);
PREP(stepEffectValue);
PREP(switchBehaviour);
PREP(switchSpeed);
PREP(switchStance);
PREP(teleportToZeuses);
PREP(toggleAIFeature);
PREP(toggleLights);
PREP(unGarrisonUnits);
PREP(uniqueGroups);