[
    QGVAR(enableKeybindUI),
    "CHECKBOX",
    ["Enable recent keybind UI", "Enable UI in bottom right that shows what keybind was used."],
    [COMPONENT_NAME],
    true
] call CBA_fnc_addSetting;

[
    QGVAR(enableKeybindErrorUI),
    "CHECKBOX",
    ["Enable keybind error UI", "Enable UI in bottom right that shows when keybind fails and why."],
    [COMPONENT_NAME],
    true
] call CBA_fnc_addSetting;

[
    QGVAR(hideUIAndHint),
    "CHECKBOX",
    ["Remove hint and keybind UI on hiding interface", "Remove hint(from your screen only) and keybind UI on hiding interface(default key: backspace)"],
    [COMPONENT_NAME],
    true,
    0,
    {
        if(_this) then
        {
            GVAR(hideUIEH) = addUserActionEventHandler ["curatorToggleInterface", "Activate", { hintSilent ""; [""] call FUNC(addKeybindUI); }];
        }
        else
        {
            removeUserActionEventHandler ["curatorToggleInterface", "Activate", GVAR(hideUIEH)];
        };
    }
] call CBA_fnc_addSetting;

[
    QGVAR(enableHealCrew),
    "CHECKBOX",
    ["Always Heal vehicle crew", "Even if only the vehicle is selected, still heal the crew of that vehicle."],
    [COMPONENT_NAME],
    true
] call CBA_fnc_addSetting;

[
    QGVAR(preferredGarrison),
    "LIST",
    ["Preferred garrison module", "Which module to use for (un)garrison units keybinds, will fallback to ZEN if LAMBS is not loaded"],
    COMPONENT_NAME,
    [[0, 1], ["ZEN", "LAMBS Danger"], 1]
] call CBA_fnc_addSetting;

[
    QGVAR(alwaysForceDelete),
    "CHECKBOX",
    ["Always Force delete", "Force delete when pressing normal delete key"],
    COMPONENT_NAME,
    true,
    0,
    {
        if(_this) then
        {
            GVAR(forceDeleteEH) = addUserActionEventHandler ["curatorDelete", "Activate", { [] call FUNC(forceDelete); }];
        }
        else
        {
            removeUserActionEventHandler ["curatorDelete", "Activate", GVAR(forceDeleteEH)];
        };
    }
] call CBA_fnc_addSetting;

[
    QGVAR(teleportOldPositionTime),
    "SLIDER",
    ["Keep old teleport position time", "For how long we should keep the old teleport position when using teleport to other zeuses keybind"],
    COMPONENT_NAME,
    [0, 120, 10, 0]
] call CBA_fnc_addSetting;