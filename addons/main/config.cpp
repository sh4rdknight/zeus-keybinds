#include "script_component.hpp"

class CfgPatches {
    class ADDON {
        name = COMPONENT_NAME;
        units[] = {};
        weapons[] = {};
        requiredVersion = REQUIRED_VERSION;
        requiredAddons[] = 
            {
                "cba_main",
                "zen_common"
            };
        authors[] = { "sh4rdknight" };
        VERSION_CONFIG;
    };
};

#include "CfgEventHandlers.hpp"
#include "CfgVersioning.hpp"
#include "ui\recentKeybindUI.hpp"