#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Switch provided units' stance.
 *
 * Arguments:
 * 0: Array of units <ARRAY>
 *
 * Return Value:
 * New text to show on UI <STRING>
 *
 * Example:
 * [_units] call zeus_keybinds_main_fnc_switchStance;
 *
*/

params ["_units"];

// Use 1st unit's stance for all so it's consistant for selection
private _newStance = "";
private _newUIText = "Unit's are ";
switch (unitPos (_units select 0)) do
{
	case "Up":
	{
		_newStance = "MIDDLE";
		_newUIText = _newUIText + "kneelling";
	};
	case "Middle":
	{
		_newStance = "DOWN";
		_newUIText = _newUIText + "prone";
	};
	default
	{
		_newStance = "UP";
		_newUIText = _newUIText + "standing";
	};
};

{
	// Set where unit is local
	["zen_common_setUnitPos", [_x, _newStance], _x] call CBA_fnc_targetEvent;
} forEach _units;
_newUIText