/*
 * Author: sh4rdknight
 * Find closest position from given array
 *
 * Arguments:
 * 0: Array of positions <ARRAY>
 * 1: Position to compare to in AGL format <ARRAY>
 *
 * Return Value:
 * 1: Index of closest position <NUMBER>
 * 2: The distance to the closest position <NUMBER>
 * 3: The closest position <ARRAY>
 *
 * Example:
 * [] call zeus_keybinds_main_fnc_getClosestPosition;
 *
 */
 
params ["_targetPosition", "_positions"];

private _closestDistance = _targetPosition vectorDistance (_positions select 0);
private _closestPositionIndex = 0;
private _closestPosition = _positions select 0;
{
	private _currentDistance = _targetPosition vectorDistance (_positions select _forEachIndex);
	if (_currentDistance < _closestDistance) then
	{
		_closestDistance = _currentDistance;
		_closestPositionIndex = _forEachIndex;
		_closestPosition = _positions select _forEachIndex;
	};
} forEach _positions;

[_closestPositionIndex, _closestDistance, _closestPosition]