/*
 * Author: sh4rdknight
 * Returns an array of unique groups that provided units are in.
 *
 * Arguments:
 * 0: Object(s) <OBJECT|ARRAY> (default: [curatorSelected select 0])
 *
 * Return Value:
 * Array of unique groups <ARRAY>
 *
 * Example:
 * [_units] call zeus_keybinds_main_fnc_uniqueGroups;
 *
 */
 
params [["_units", curatorSelected select 0, []]];

private _groups = [];
{
    if((!isPlayer _x) && (alive _x)) then
    {
        private _group = group _x;
        if (_group isNotEqualTo grpNull) then
        {
            _groups pushBackUnique _group;
        }; 
    };
} forEach _units;

_groups