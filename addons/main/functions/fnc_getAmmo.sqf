#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Looks through given object and returns an array containg each magazine class and ammo count for it.
 *
 * Arguments:
 * 0: Target object <OBJECT>
 *
 * Return Value:
 * Array of arrays with magazine's class and current ammo <ARRAY>
 *
 * Example:
 * [_unit] call zeus_keybinds_main_fnc_getAmmo;
 *
 */

params ["_target"];

// Check if it's a unit,
if (_target isKindOf "CAManBase") then
{
	[_target] call FUNC(getUnitAmmo)
}
else
{
	// if not it must be a vehicle.
	[_target] call FUNC(getVehicleAmmo)
};