#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Garrisons provided units with radius around position.
 *
 * Arguments:
 * 0: Array of units <ARRAY>
 * 1: Position to garrison around <ARRAY>
 * 2: Radius to garrison around position <NUMBER>
 *
 * Return Value:
 * None
 *
 * Example:
 * ["_units", "_position", 30] call zeus_keybinds_main_fnc_garrisonUnits;
 *
 */

params ["_units", "_position", "_radius"];

// If LAMBS is loaded and preferred
if (GVAR(lambsWPLoaded) && GVAR(preferredGarrison) == 1) then
{
    private _groups = [_units] call FUNC(uniqueGroups);
    {
        [_x, true, true] call lambs_wp_fnc_taskReset;
        [_x, _position, _radius, [], true, false, 0] call lambs_wp_fnc_taskGarrison;

        // Remove first waypoint, as taskGarrison adds an unnecessary move waypoint
        deleteWaypoint [_x, 0];
    } forEach _groups;
}
else
{
    // otherwise use ZEN garrison.
    [_units, ASLToAGL _position, _radius, 0, false] call zen_ai_fnc_garrison;
};