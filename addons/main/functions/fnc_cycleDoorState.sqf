/*
 * Author: sh4rdknight
 * Cycles the state of the door and returns the new state
 *
 * Arguments:
 * 0: Target vehicle <OBJECT>
 *
 * Return Value:
 * New door state <String>
 *
 * Example:
 * [_nearestBuilding, _doorIndex] call zeus_keybinds_main_fnc_cycleDoorState;
 *
 */
params ["_nearestBuilding", "_doorIndex"];

// Read the state before cycling as state depends on animation progress
private _newState = [_nearestBuilding, _doorIndex] call zen_doors_fnc_getState;
_newState = _newState + 1;
if (_newState > 2) then
{
	_newState = 0;
};

[_nearestBuilding, _doorIndex, _newState] call zen_doors_fnc_setState;

switch (_newState) do
{
	case 0: { "Door is closed" };
	case 1: { "Door is locked" };
	case 2: { "Door is open" };
};