#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Handles reselection when units change side
 *
 * Arguments:
 * 0: Units that are changing side <ARRAY>
 *
 * Return Value:
 * None
 *
 * Example:
 * [_units] call zeus_keybinds_main_fnc_handleUnitsSideChanged;
 *
 */

params ["_units", "_delay"];

// We will need to remove units from selection as they switch sides
setCuratorSelected ((curatorSelected select 0) - _units);

// When units switch sides they get removed from selection but it's not instant so wait some time
[{ addCuratorSelected (_this)}, _units, _delay] call CBA_fnc_waitAndExecute;