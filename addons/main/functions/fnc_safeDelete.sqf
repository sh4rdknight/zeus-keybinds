#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Safely deletes given objects.
 *
 * Arguments:
 * 0: Objects to delete <ARRAY>
 *
 * Return Value:
 * None
 *
 * Example:
 * [_objects] call zeus_keybinds_main_fnc_safeDelete;
 *
 */

params ["_objects"];
{
    private _objectCrew = crew _x;
    // if there is valid crew(not just object itself),
    if (count _objectCrew > 0 && (_objectCrew find _x) isEqualTo -1) then
    {
        // delete the crew safely first.
        deleteVehicleCrew _x;
    };
    // delete the object
    deleteVehicle _x;
} forEach _objects;