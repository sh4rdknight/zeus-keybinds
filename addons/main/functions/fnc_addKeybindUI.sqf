#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Show a text box in bottom left corner with the provided text.
 *
 * Arguments:
 * 0: Text to display <STRING>
 * 1: Which error string to display(overrides text) <NUMBER> (default: -1, no error)
 *
 * Return Value:
 * None
 *
 * Example:
 * ["HELLO"] call zeus_keybinds_main_fnc_addKeybindUI;
 *
 */

params [["_text", "", [""]], ["_errorNumber", -1, [0]]];

// Always remove the old UI, as otherwise will show "ERROR"
"zeus_keybinds_main_recentKeybind_layer" cutText ["", "PLAIN"];

// Override text if we need to display an error
if ((_errorNumber != -1) && GVAR(enableKeybindErrorUI)) then
{
	switch (_errorNumber) do 
	{
		case 0: { _text = "No object(s)" };
		case 1: { _text = "No unit(s)" };
		case 2: { _text = "No building(s)" };
		case 3: { _text = "Nothing selected" };
		case 4: { _text = "No door(s)" };
		case 11: { _text = "Invalid target(s)" };
		case 12: { _text = "No target(s)" };
		default { _text = "" };
	};
};

// Don't display empty text or if player disabled UI and there is no error code
if ((count _text isEqualTo 0) || (!GVAR(enableKeybindUI) && (_errorNumber == -1))) exitWith {};

// Create the UI
"zeus_keybinds_main_recentKeybind_layer" cutRsc ["zeus_keybinds_main_recentKeybind_display", "PLAIN", 0, true];

// Set the text
private _display = uiNamespace getVariable "zeus_keybinds_main_recentKeybind_display";
private _ctrlTitle = _display displayCtrl IDC_RECENT_KEYBIND;
_ctrlTitle ctrlSetText _text;