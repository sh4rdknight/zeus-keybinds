#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * If map is open removes marker below cursor, otherwise force deletes selected objects.
 *
 * Arguments:
 * None
 *
 * Return Value:
 * None
 *
 * Example:
 * [] call zeus_keybinds_main_fnc_canUseKeybind;
 *
 */

if (visibleMap) then
{
	// Zeus map control
	private _mouseOver = ctrlMapMouseOver ((findDisplay 312) displayCtrl 50);
	if (_mouseOver select 0 == "marker") then 
	{
		deleteMarker (_mouseOver select 1);
	};
} 
else 
{
	private _objects = (curatorSelected select 0);
	if (count _objects isNotEqualTo 0) then 
	{
		[_objects] call FUNC(safeDelete);
	};
};