#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Switches waypoints' and groups' speed between full -> limited -> normal -> full....
 *
 * Arguments:
 * 0: Array of groups <ARRAY>
 *
 * Return Value:
 * New text to show on UI <STRING>
 *
 * Example:
 * [_groups, _waypoints] call zeus_keybinds_main_fnc_switchSpeed;
 *
*/

params ["_groups", "_waypoints"];

private _currentSpeed = "";
private _newUIText = "";
if (count _groups isNotEqualTo 0) then 
{ 
    _currentSpeed = speedMode (_groups select 0);
} 
else
{
    if (count _waypoints isNotEqualTo 0) then
    {
        _currentSpeed = waypointSpeed (_waypoints select 0);
    }
    else
    {
        // Empty arguments, return empty string
        _newUIText
    };
};

// Use 1st unit's stance for all so it's consistant for selection
private _newSpeed = "";
_newUIText = "Speed is ";
switch (_currentSpeed) do
{
	case "FULL":
	{
		_newSpeed = "LIMITED";
		_newUIText = _newUIText + "limited";
	};
	case "LIMITED":
	{
		_newSpeed = "NORMAL";
		_newUIText = _newUIText + "normal";
	};
	default
	{
		_newSpeed = "FULL";
		_newUIText = _newUIText + "full";
	};
};

{
	// If this is current waypoint for a group,
	if ((_x select 1) isEqualTo 1) then
	{
		// Add group to be updated too, otherwise waypoint won't affect anything
		_groups pushBackUnique (_x select 0)
	};
	// Call on server
	["zen_common_setWaypointSpeed", [_x, _newSpeed], _x] call CBA_fnc_serverEvent;
} forEach _waypoints;

{
	// Set where unit is local
	["zen_common_setSpeedMode", [_x, _newSpeed], _x] call CBA_fnc_targetEvent;
} forEach _groups;


_newUIText