#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Returns either already selected objects or objects in radius around cursor with optional filter with typeName of objects.
 *
 * Arguments:
 * 0: Only select in radius around cursor <BOOL> (default: false)
 * 1: typeName of objects to filter for <STRING> (default: "")
 *
 * Return Value:
 * Array of objects <ARRAY>
 *
 * Example:
 * ["Building"] call zeus_keybinds_main_fnc_getSelection;
 *
 */
params [["_onlyInRadius", false, [false]], ["_typeName", "", [""]]];

private _objects = (curatorSelected select 0);
// If we have nothing selected or we only want objects in radius
if (count _objects isEqualTo 0 || _onlyInRadius) then
{
    // then find objects in radius.
    private _position = ASLToAGL ([] call zen_common_fnc_getPosFromScreen);
    _objects = _position nearObjects GVAR(effectValue);
};

// Filter the objects
if (_typeName isNotEqualTo "") then
{
    _objects = _objects select { _x isKindOf _typeName};
};

_objects