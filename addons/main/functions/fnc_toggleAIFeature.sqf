/*
 * Author: sh4rdknight
 * Toggle given feature on provided units.
 *
 * Arguments:
 * 0: Units to toggle the feature on <ARRAY> (default: [])
 * 1: Feature to toggle <STRING> (default: "")
 *
 * Return Value:
 * Was feature enabled <BOOL>
 *
 * Example:
 * [_units, "PATH"] call zeus_keybinds_main_fnc_toggleAIFeature;
 *
 */

params [["_units", "[]", [[]]], ["_aiFeature", "", [""]]];
private _enabledFeature = (_units select 0) checkAIFeature _aiFeature;
{
    // Make sure it's a unit
    if (_x isKindOf "CAManBase") then
    {
        // Make sure it's alive and not a player
        if ((alive _x) && !(isPlayer _x)) then
        {
            if (_enabledFeature) then
            {
                // disable where the unit is local.
                ["zen_common_disableAI", [_x, _aiFeature], _x] call CBA_fnc_targetEvent;
            }
            else
            {
                // enable where the unit is local.
                ["zen_common_enableAI", [_x, _aiFeature], _x] call CBA_fnc_targetEvent;
            };
        };
    }
    else
    {
         // If not check if the object has valid crew(not only itself)
        private _unitCrew = crew _x;
        if (count _unitCrew > 0 && (_unitCrew find _x) isEqualTo -1) then
        {
            {
                // Only add the crew if they are not already in the list
                _units pushBackUnique _x;
            } forEach _unitCrew;
        };
    }
} forEach _units;

!_enabledFeature