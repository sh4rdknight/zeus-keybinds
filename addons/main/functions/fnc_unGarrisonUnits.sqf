#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Ungarrisons provided units.
 *
 * Arguments:
 * 0: Array of units <ARRAY>
 *
 * Return Value:
 * None
 *
 * Example:
 * ["_units"] call zeus_keybinds_main_fnc_unGarrisonUnits;
 *
 */

params ["_units"];

// If LAMBS is loaded and preferred
if (GVAR(lambsWPLoaded) && GVAR(preferredGarrison) == 1) then
{
    private _groups = [_units] call FUNC(uniqueGroups);
    {
        [_x, true, true] call lambs_wp_fnc_taskReset;

    } forEach _groups;
}
else
{
    // ZEN ungarrison where units are local.
    ["zen_ai_unGarrison", _units, _units] call CBA_fnc_targetEvent;
};