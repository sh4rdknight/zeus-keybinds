/*
 * Author: sh4rdknight
 * Attaches curator camera to target object, detaches camera when object has been unselected.
 *
 * Arguments:
 * 0: Target <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [_player] call zeus_keybinds_main_fnc_followTarget;
 *
 */

params ["_target"];

// get the size of target
private _bbr = boundingBoxReal vehicle _target;
private _p1 = _bbr select 0;
private _p2 = _bbr select 1;
private _maxLength = abs ((_p2 select 1) - (_p1 select 1));
_maxLength = _maxLength * -2;
curatorCamera attachTo [_target, [0, _maxLength, 1]];

[{
	// if unit is deselected
    !(_this in (curatorSelected select 0));
}, {
	detach curatorCamera;
	// reset camera rotation
	curatorCamera setVectorDirAndUp [(vectorDir _this), [0,0,1]];
}, _target] call CBA_fnc_waitUntilAndExecute