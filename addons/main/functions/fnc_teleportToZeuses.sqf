#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Teleport to other zeuses location, or teleport back to your previous position
 *
 * Return Value:
 * New text to show on UI <STRING>
 *
 * Example:
 * [] call zeus_keybinds_main_fnc_teleportToZeuses;
 *
 */

private _curatorsNum = count allCurators;
// Setup for first time, start from yourself
if (GVAR(ZeusTPLastUsed) == 0) then
{
	for "_i" from 0 to (_curatorsNum - 1) do {
		private _targetCurator = getAssignedCuratorUnit (allCurators select _i);
		if ((getAssignedCuratorLogic player) isEqualTo (getAssignedCuratorLogic _targetCurator)) then
		{
			GVAR(ZeusTPIndex) = _i;
			GVAR(ZeusTPIndexDefault) = _i;
			break;
		};
	};
};

// Check if we should reset the index and save position
if (time - GVAR(ZeusTPLastUsed) > GVAR(teleportOldPositionTime) || GVAR(ZeusTPLastUsed) == 0) then
{
	GVAR(ZeusTPLastPos) = [getPosASL curatorCamera, vectorDir curatorCamera, vectorUp curatorCamera];
	GVAR(ZeusTPIndex) = GVAR(ZeusTPIndexDefault);
};

GVAR(ZeusTPIndex) = (GVAR(ZeusTPIndex) + 1) % _curatorsNum;
private _targetCurator = getAssignedCuratorUnit (allCurators select GVAR(ZeusTPIndex));

// If we looped around, restore position
if (getAssignedCuratorLogic player isEqualTo getAssignedCuratorLogic _targetCurator) then
{
	curatorCamera setPosASL (GVAR(ZeusTPLastPos) select 0);
	curatorCamera setVectorDirAndUp [(GVAR(ZeusTPLastPos) select 1), (GVAR(ZeusTPLastPos) select 2)];
	GVAR(ZeusTPLastUsed) = time;
	"Restored position"
} 
else {
	["zen_common_execute", [{
		["zen_common_execute", [{
			params ["_posASL", "_vectorDir", "_vectorUp"];
			curatorCamera setPosASL _posASL;
			curatorCamera setVectorDirAndUp [_vectorDir, _vectorUp];
		}, [getPosASL curatorCamera, vectorDir curatorCamera, vectorUp curatorCamera]], _this] call CBA_fnc_ownerEvent; // Send up private position for teleport
	}, clientOwner], _targetCurator] call CBA_fnc_targetEvent;
	GVAR(ZeusTPLastUsed) = time;
	"Teleported to Zeus"
};