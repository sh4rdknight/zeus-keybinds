#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Heals provided objects.
 *
 * Arguments:
 * 0: Objects to heal <ARRAY>
 *
 * Return Value:
 * None
 *
 * Example:
 * [_players] call zeus_keybinds_main_fnc_healObjects;
 *
 */

params ["_objects", ["_onlyBuildings", false]];
{
    // Check if target is still alive,
    if (alive _x) then
    {
        // Is it a person,
        if (_x isKindOf "CAManBase" && !_onlyBuildings) then
        {
            // use healUnit in case ACE medical is used.
            [_x] call zen_common_fnc_healUnit;
        }
        else
        {
            // Check if it's ruins that have the real damaged model below
            if (_x isKindOf "Ruins") then
            {
                // Find and heal the real building only
                private _buildingPos = ASLToAGL (getPosASL _x);
                private _building = (nearestObjects [_buildingPos, ["House"], 5, true]) select 0;
                _building setDamage 0;
            }   
            else 
            {
                if (!_onlyBuildings) then
                {
                    // if not just repair the object.
        	        _x setDamage 0;
                    
                    if(GVAR(enableHealCrew)) then
                    {
                        private _unitCrew = crew _x;
        	            {
                            _objects pushBackUnique _x;
                        } forEach _unitCrew;
                    };
                };
            };
        };
    }
    else
    {
        // Only heal it if it's a destroyed building
        if (_x isKindOf "Building") then
        {
            _x setDamage 0;
        };
    };
} forEach _objects;