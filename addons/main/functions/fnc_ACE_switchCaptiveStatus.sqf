#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Cycles selected units between surrendering -> handcuffed -> free -> surrenderring -> ...
 * Requires ACE mod
 *
 * Arguments:
 * 0: Array of units <ARRAY>
 *
 * Return Value:
 * New text to show on UI <STRING>
 *
 * Example:
 * [_units] call zeus_keybinds_main_fnc_ACE_switchCaptiveStatus;
 *
*/

params ["_units"];

private _unitCheck = _units select 0;
private _newState = true;
private _newCaptive = "";
private _newUIText = "Now ";
if (_unitCheck getVariable ["ace_captives_isSurrendering", false]) then
{
    _newUIText = _newUIText + "handcuffed";
    _newCaptive = "ace_captives_setHandcuffed";
} 
else
{
    if (_unitCheck getVariable ["ace_captives_isHandcuffed", false]) then
    {
		_newUIText = _newUIText + "free";
        _newCaptive = "ace_captives_setHandcuffed";
        _newState = false;
    }
	else
	{
		_newUIText = _newUIText + "surrendering";
        _newCaptive = "ace_captives_setSurrendered";
	};
};

// If units changed side
if (_newCaptive == "ace_captives_setSurrendered" || !_newState) then
{
    [_units, 0.05] call FUNC(handleUnitsSideChanged);
};


{
    [_newCaptive, [_x, _newState], _x] call CBA_fnc_targetEvent;
} forEach _units;


_newUIText