/*
 * Author: sh4rdknight
 * Find buildings that have doors in radius around center position 
 *
 * Arguments:
 * 0: Center AGL position to search around <ARRAY>
 * 1: Radius to search in <NUMBER>
 *
 * Return Value:
 * Building with doors sorted by proximity <ARRAY>
 *
 * Example:
 * [_mousePos, 200] call zeus_keybinds_main_fnc_getBuildingsWithDoors;
 *
 */
params ["_position", "_radius"];

// Use nearestObjects in case building was spawned in the editor
private _doorBuildings = nearestObjects [_position, ["House", "Building"], _radius];
// Filter for buildings with doors
_doorBuildings = _doorBuildings select {getNumber ((configFile >> "CfgVehicles" >> typeOf _x) >> "numberofdoors") > 0};

_doorBuildings