/*
 * Author: sh4rdknight
 * Looks through given vehicle and returns an array containg each magazine class and ammo count for it.
 *
 * Arguments:
 * 0: Target vehicle <OBJECT>
 *
 * Return Value:
 * Array of arrays with magazine's class and current ammo <ARRAY>
 *
 * Example:
 * [_vehicle] call zeus_keybinds_main_fnc_getVehicleAmmo;
 *
 */

params ["_vehicleTarget"];

private _magazinesNames = [];
private _magazinesAmmo = [];
// Go through the magazines
{
    // create variables for needed parameters
    _x params ["_magazineClass", "", "_currentAmmo"];

    // Make sure it has valid name not it's not a laser
    if (_magazineClass isNotEqualTo "" && _magazineClass isNotEqualTo "Laserbatteries") then 
	{
		private _insertIndex = _magazinesNames pushBackUnique _magazineClass;
		// Check if we already have the magazine,
		if (_insertIndex isEqualTo -1) then
		{
			// if so just update the ammo count.
			_insertIndex = _magazinesNames find _magazineClass;
			private _updatedAmmo = (_magazinesAmmo select _insertIndex) + _currentAmmo;
			_magazinesAmmo set [_insertIndex, _updatedAmmo];
		}
		else
		{
			// if not append ammo too.
			_magazinesAmmo pushBack _currentAmmo;
		};
	};
} forEach magazinesAllTurrets _vehicleTarget;

// Construct the return array
private _readoutFinal = [];
{
	_readoutFinal pushBack [_x, (_magazinesAmmo select _forEachIndex)];
} forEach _magazinesNames;

_readoutFinal