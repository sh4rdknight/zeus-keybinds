#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Switches waypoints' and groups' behaviour between careless -> safe -> combat -> careless...
 *
 * Arguments:
 * 0: Array of groups <ARRAY>
 * 1: Array of waypoints <ARRAY>
 *
 * Return Value:
 * New text to show on UI <STRING>
 *
 * Example:
 * [_groups, _waypoints] call zeus_keybinds_main_fnc_switchBehaviour;
 *
 */

params ["_groups", "_waypoints"];

private _currentBehaviour = "";
private _newUIText = "";
if (count _groups isNotEqualTo 0) then 
{ 
    _currentBehaviour = behaviour leader (_groups select 0);
} 
else
{
    if (count _waypoints isNotEqualTo 0) then
    {
        _currentBehaviour = waypointBehaviour (_waypoints select 0);
    }
    else
    {
        // Empty arguments, return empty string
        _newUIText
    };
};

private _newBehaviour = "";
private _newSpeedMode = "";
// Use 1st group to determine what mode to switch to
switch (_currentBehaviour) do {
    case "AWARE";
    case "COMBAT":
    {     
        _newBehaviour = "CARELESS";
        _newSpeedMode = "LIMITED";
        _newUIText = "Set to Careless";
    };
    case "CARELESS":
    {
        _newBehaviour = "SAFE";
        _newSpeedMode = "LIMITED";
        _newUIText = "Set to Safe";
    };
    default 
    { 
        _newBehaviour = "COMBAT";
        _newSpeedMode = "FULL";
        _newUIText = "Set to Combat";
    };
};

{
    // If this is current waypoint for a group,
	if ((_x select 1) isEqualTo 1) then
	{
		// Add group to be updated too, otherwise waypoint won't affect anything
		_groups pushBackUnique (_x select 0)
	};
	// call on server
	["zen_common_setWaypointBehaviour", [_x, _newBehaviour], _x] call CBA_fnc_serverEvent;
	["zen_common_setWaypointSpeed", [_x, _newSpeedMode], _x] call CBA_fnc_serverEvent;
} forEach _waypoints;

{
	// set where group is local
	["zen_common_setBehaviour", [_x, _newBehaviour], _x] call CBA_fnc_targetEvent;
	["zen_common_setSpeedMode", [_x, _newSpeedMode], _x] call CBA_fnc_targetEvent;
} forEach _groups;

_newUIText