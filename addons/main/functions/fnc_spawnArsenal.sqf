#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Spawns diffrent arsenal depending on loaded mods.
 *
 * Arguments:
 * 0: Position ATL <ARRAY>
 *
 * Return Value:
 * None
 *
 * Example:
 * [position] call zeus_keybinds_main_fnc_spawnArsenal;
 *
 */

params ["_position"];

// Create invincible box
private _box = createVehicle ["B_supplyCrate_F", _position, [], 0, "NONE"];
_box allowDamage false;
// Align the box with the terrain underneath
_box setVectorUp surfaceNormal position _box;
[[_box]] call FUNC(clearInventory);

// Add it to all curators' editable objects list
[_box, true] call zen_common_fnc_updateEditableObjects;

// Check if we are on FKGaming.eu servers,
if(!isNil "fkf_main_fnc_addArsenal") then
{
	// if so use the filtered arsenal.
    [_box, 1] call fkf_main_fnc_addArsenal;
}
else
{
    // If ACE arsenal is loaded and is preffered
    if (GVAR(aceArsenalLoaded) && zen_common_preferredArsenal == 1) then
    {
        [_box, true] call ace_arsenal_fnc_removeBox;
        [_box, true, true] call ace_arsenal_fnc_initBox;
    }
    else
    {
		// otherwise use virtual arsenal.
        ["AmmoboxInit", [_box, true]] call BIS_fnc_arsenal;
    };
};