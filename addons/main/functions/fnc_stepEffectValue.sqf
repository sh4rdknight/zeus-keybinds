#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Increases the effectValue using steps.
 *
 * Arguments:
 * 0: Change in steps <NUMBER>
 *
 * Return Value:
 * New text to show on UI <STRING>
 *
 * Example:
 * [-1] call zeus_keybinds_main_fnc_stepEffectValue;
 *
 */

params ["_deltaSteps"];

private _maxStep = (count EFFECT_STEPS) - 1;

// Get current step
private _currentStep = EFFECT_STEPS find GVAR(effectValue);
_currentStep = round (_currentStep + _deltaSteps);

// Make sure we are in range of the array
if (_maxStep < _currentStep) then
{
	_currentStep = _maxStep;
};
if (_currentStep < 0) then
{
	_currentStep = 0;
};

// Show the updated value
GVAR(effectValue) = EFFECT_STEPS select _currentStep;
private _newUIText = "Current - "+ str GVAR(effectValue);
_newUIText