#include "script_component.hpp"
/*
 * Author: sh4rdknight
 * Toggle lights on given objects.
 *
 * Arguments:
 * 0: Array of objects <ARRAY>
 *
 * Return Value:
 * None
 *
 * Example:
 * [_units] call zeus_keybinds_main_fnc_toggleLights;
 *
 */

params ["_objects"];

private _unitsToggled = false;
{
    // Check if it's a movable object
    if (_x isKindOf "AllVehicles") then
    {
        // Is it a person
        if (_x isKindOf "CAManBase") then
        {
            // If we haven't switched lights on units yet
            if (!_unitsToggled) then
            {
                // Do it on all of the groups now as multiple objects might be in the same group
                private _groups = [_objects] call FUNC(uniqueGroups);
                {
                    // Check if lights are on by checking group leader
                    if (leader _x isFlashlightOn (currentWeapon leader _x)) then
                    {
                        ["zen_common_enableGunLights", [_x, "ForceOff"], _x] call CBA_fnc_targetEvent;
                    }
                    else
                    {
                        ["zen_common_enableGunLights", [_x, "ForceOn"], _x] call CBA_fnc_targetEvent;
                    };
                } forEach _groups;
                _unitsToggled = true;
            };
        }
        else
        {
            // Disable AI automatic light switching to have an effect
            ["zen_common_disableAI", [_x,  "LIGHTS"], _x] call CBA_fnc_targetEvent;
    
            // Toggle headlights
            private _bIsLightOn = isLightOn _x;
            ["zen_common_setPilotLight", [_x, !_bIsLightOn], _x] call CBA_fnc_targetEvent;
        };
    }
    else
    {
        // Toggle light on object if it has a light source
        if (_x call zen_common_fnc_getLightingSelections isNotEqualTo []) then 
        {
            // Make sure it's a valid light
            private _bIsLightOn = lightIsOn _x;
            if (_bIsLightOn isNotEqualTo "ERROR") then
            {
                // Generate unique id for this objects
                private _jipID = format ["Zeus_Keybinds_ToggleLights:%1", _x call BIS_fnc_netId];
                if (_bIsLightOn isEqualTo "ON" || _bIsLightOn isEqualTo "AUTO") then
                {
                    // Disable lights
                    ["zen_common_execute", [zen_common_fnc_setLampState, [_x, false, true]], _jipID] call CBA_fnc_globalEventJIP;
                }
                else
                {
                    // Enable lights
                    ["zen_common_execute", [zen_common_fnc_setLampState, [_x, true, true]], _jipID] call CBA_fnc_globalEventJIP;
                };
                // Set to delete JIP when object is deleted
                [_jipID, _x] call CBA_fnc_removeGlobalEventJIP;
            };
        };
    };
} forEach _objects;