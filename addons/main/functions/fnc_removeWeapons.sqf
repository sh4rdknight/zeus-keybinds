/*
 * Author: sh4rdknight
 * Drops all weapons of selected units and removes ammo for vehicles.
 *
 * Arguments:
 * 0: Array of units <ARRAY>
 *
 * Return Value:
 * None
 *
 * Example:
 * ["_units"] call zeus_keybinds_main_fnc_removeWeapons;
 *
 */

params ["_units"];

_units = _units select {(_x isKindOf "AllVehicles") && (alive _x)};
{
	if (_x isKindOf "CAManBase") then 
	{
		private _unit = _x;
		// Remember current animation to restore it
		private _currentAnimation = animationState _unit;

		private _wArray = weapons _unit;
		{
			[_unit, _x] call CBA_fnc_dropWeapon;
		} forEach _wArray;

		// Reset the animation back to the previous one
		_unit switchMove _currentAnimation;
	} 
	else 
	{
		// Should be a vehicle so remove ammo
		[_x, 0] call zen_common_fnc_setVehicleAmmo;
	};
} forEach _units;