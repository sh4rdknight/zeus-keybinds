/*
 * Author: sh4rdknight
 * Clears inventory of provided containers.
 *
 * Arguments:
 * 0: Containers <ARRAY>
 *
 * Return Value:
 * None
 *
 * Example:
 * [_vehicles] call zeus_keybinds_main_fnc_clearInventory;
 *
 */

params ["_containers"];

{
	// clear the inventory.
	clearItemCargoGlobal _x;
	clearMagazineCargoGlobal _x;
	clearWeaponCargoGlobal _x;
	clearBackpackCargoGlobal _x;
} forEach _containers;