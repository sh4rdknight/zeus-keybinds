/*
 * Author: sh4rdknight
 * Checks if we have a curator camera and currently not typing in the search bar.
 *
 * Arguments:
 * None
 *
 * Return Value:
 * Should trigger keybind <BOOL>
 *
 * Example:
 * [] call zeus_keybinds_main_fnc_canUseKeybind;
 *
 */
 
(!isNull curatorCamera && {!(missionNamespace getVariable["RscDisplayCurator_search", false])});