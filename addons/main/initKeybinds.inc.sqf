#include "\a3\ui_f\hpp\defineDIKCodes.inc"

[COMPONENT_NAME, QGVAR(IncreaseEffectValue), ["Increase effect value", "Increase the radius of garrison area, the time AI suppresses cursor's position, etc."],
{
    if (call FUNC(canUseKeybind)) then
    {
        [[1] call FUNC(stepEffectValue)] call FUNC(addKeybindUI);
        true; // Still consume input
    };
}, "", [DIK_PRIOR, [false, true, true]]] call CBA_fnc_addKeybind; // Default: CTRL + ALT + PAGE UP


[COMPONENT_NAME, QGVAR(DecreaseEffectValue), ["Decrease effect value", "Decrease the radius of garrison area, the time AI suppresses cursor's position, etc."],
{
    if (call FUNC(canUseKeybind)) then
    {
        [[-1] call FUNC(stepEffectValue)] call FUNC(addKeybindUI);
        true; // Still consume input
    };
}, "", [DIK_NEXT, [false, true, true]]] call CBA_fnc_addKeybind; // Default: CTRL + ALT + PAGE DOWN


[COMPONENT_NAME, QGVAR(ClearInventory), ["Clear inventory of selected", "Clears inventory of selected(excluding inventory of people)"],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = (curatorSelected select 0);
        _objects = _objects select { !(_x isKindOf "CAManBase") };
        if (count _objects isNotEqualTo 0) then
        {
            [_objects] call FUNC(clearInventory);
            ["Cleared inventories"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_I, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + I


[COMPONENT_NAME, QGVAR(CycleDoorState), ["Cycle door's state", "Cycles closest door to the cursor between being closed -> locked -> open -> closed..."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _position = ASLToAGL ([] call zen_common_fnc_getPosFromScreen);
        private _nearestBuilding = ([_position, 200] call FUNC(getBuildingsWithDoors));
        if (count _nearestBuilding isNotEqualTo 0) then
        {
            _nearestBuilding = _nearestBuilding select 0;
            private _doorPositions = [_nearestBuilding] call zen_doors_fnc_getDoors;
            // Convert from relative to building to world position
            private _doorPositions = _doorPositions apply {_nearestBuilding modelToWorldVisual _x};
            private _closestDoor = ([_position, _doorPositions] call FUNC(getClosestPosition));
            // If door is too far from cursor, don't continue
            if (_closestDoor select 1 > 5) exitWith
            {
                ["", 4] call FUNC(addKeybindUI);
            };

            // For some reason you need to add 1 to the door index
            private _doorIndex =  (_closestDoor select 0) + 1;
            ([_nearestBuilding, _doorIndex] call FUNC(cycleDoorState)) call FUNC(addKeybindUI);
        }
        else
        {
            ["", 2] call FUNC(addKeybindUI);
        };
        true; // Still consume input
    };
}, "", [DIK_B, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + B


[COMPONENT_NAME, QGVAR(DestroyBuildings), ["Destroy buildings in radius", "Destroy buildings in radius around cursor."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = [true, "Building"] call FUNC(getSelection);
        if (count _objects isNotEqualTo 0) then
        {
            [_objects, 4, false] call zen_modules_fnc_moduleDamageBuildings;
            ["Destroyed buildings"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 2] call FUNC(addKeybindUI); 
        };
        
        true; // Still consume input
    };
}, "", [DIK_END, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + END


[COMPONENT_NAME, QGVAR(HealSelected), ["Heal selected", "Heal and repairs selected objects"],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = (curatorSelected select 0);
        if (count _objects isNotEqualTo 0) then
        {
            [_objects] call FUNC(healObjects);
            ["Healed selected"] call FUNC(addKeybindUI);
        }
        else    
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_H, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + H


[COMPONENT_NAME, QGVAR(RepairBuildings), ["Repair buildings in radius", "Repairs buildings in radius around the cursor"],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = [true] call FUNC(getSelection);
        if (count _objects isNotEqualTo 0) then
        {
            [_objects, true] call FUNC(healObjects);
            ["Repaired in radius"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_H, [false, true, true]]] call CBA_fnc_addKeybind; // Default: CTRL + ALT + H


[COMPONENT_NAME, QGVAR(ResupplyVehicles), ["Rearm selected or spawn an arsenal", "Will try to rearm selected, if nothing is selected will spawn an arsenal at cursor's position"],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _units = (curatorSelected select 0);
        // If we have something selected,
        if (count _units isNotEqualTo 0) then
        {
            // Filter for vehicles that are alive
            _units = _units select { (alive _x && (_x isKindOf "AllVehicles") && !(_x isKindOf "CAManBase")) };
            if (count _units isNotEqualTo 0) then
            {
                // rearm filtered vehicles.
                {
                    [_x, 1] call zen_common_fnc_setVehicleAmmo;
                } forEach _units;
                ["Rearmed selected"] call FUNC(addKeybindUI);
            }
            else
            { 
                ["", 11] call FUNC(addKeybindUI); 
            };
        }
        else
        {
            // otherwise spawn an arsenal at cursor.
            private _position = ASLToATL ([] call zen_common_fnc_getPosFromScreen);
            [_position] call FUNC(spawnArsenal);
            ["Arsenal spawned"] call FUNC(addKeybindUI);
        };
        true; // Still consume input
    };
}, "", [DIK_R, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + R


[COMPONENT_NAME, QGVAR(UnequipWeapons), ["Remove ammo and drop units' weapons", "Removes vehicles' ammo and drops all weapons of selected units"],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _units = (curatorSelected select 0);
        _units = _units select { (alive _x) && (_x isKindOf "AllVehicles")};
        if (count _units isNotEqualTo 0) then
        {
            [_units] call FUNC(removeWeapons);
            ["Removed weapons"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 11] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_U, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + U


[COMPONENT_NAME, QGVAR(ShowSelectedsAmmo), ["Show ammo of selected", "Shows how much ammo of each type the selected has left"],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = (curatorSelected select 0);
        _objects = _objects select { (alive _x) && (_x isKindOf "AllVehicles") };
        if (count _objects isNotEqualTo 0) then
        {
            // Show visually which unit we used
            setCuratorSelected [_objects select 0];

            private _magazinesAmmo = [_objects select 0] call FUNC(getAmmo);
            private _cfgMagazines = configFile >> "CfgMagazines";
            private _hintString = "";
            {
                private _magazineName = getText (_cfgMagazines >> _x select 0 >> "displayName");
                // Don't show types with empty names
                if (_magazineName isNotEqualTo "") then {
                    _hintString = _hintString + _magazineName + ": " + (str (_x select 1)) + "\n";
                };
            } forEach _magazinesAmmo;
            hintSilent _hintString;
            ["Show ammo"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 11] call FUNC(addKeybindUI); 
        };
        true; // Stil consume input
    };
}, "", [DIK_LBRACKET, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + [


[COMPONENT_NAME, QGVAR(SpawnControlUnit), ["Spawn a ghost unit at cursor and control it", "Spawns invincible, invisible unit and remote controls it, deleted after."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _position = ASLToAGL ([] call zen_common_fnc_getPosFromScreen);
        private _agent = createAgent ["C_man_1", _position, [], 0, "CAN_COLLIDE"];
        GVAR(controlledUnit) = _agent;
        ["zen_common_hideObjectGlobal", [_agent, true]] call CBA_fnc_serverEvent;
        ["zen_common_allowDamage", [_agent, false], _agent] call CBA_fnc_targetEvent;
        // So other curators can see you
        [_agent, false, getAssignedCuratorLogic player] call zen_common_fnc_updateEditableObjects;
        _agent enableStamina false;
        _agent enableFatigue false;

        // Remote control
        GVAR(controlledUnit) call zen_remote_control_fnc_start;
        GVAR(remoteControlEh) = ["zen_remoteControlStopped", { deleteVehicle GVAR(controlledUnit); ["zen_remoteControlStopped", GVAR(remoteControlEh)] call CBA_fnc_removeEventHandler; }] call CBA_fnc_addEventHandler;
    };
    true; // Still consume input
}, "", [DIK_SEMICOLON, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + ;