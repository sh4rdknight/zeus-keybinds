#include "\a3\ui_f\hpp\defineDIKCodes.inc"
private _CategoryName = "Editor";

[[COMPONENT_NAME, _CategoryName], QGVAR(AddEditableObjects), ["Add objects in radius to editable list", "Add objects in radius around cursor to your editable list."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = [true] call FUNC(getSelection);
        if (count _objects isNotEqualTo 0) then
        {
            [_objects, true, getAssignedCuratorLogic player] call zen_common_fnc_updateEditableObjects;
            ["Added editable"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_EQUALS, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + =


[[COMPONENT_NAME, _CategoryName], QGVAR(RemoveEditableObjects), ["Remove selected objects or in radius from editable list", "Remove selected objects or in radius around the cursor from the your editable list."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = [] call FUNC(getSelection);
        if (count _objects isNotEqualTo 0) then
        {
            [_objects, false, getAssignedCuratorLogic player] call zen_common_fnc_updateEditableObjects;
            ["Removed editable"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_MINUS, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + -(Not numpad one)


[[COMPONENT_NAME, _CategoryName], QGVAR(AddSelectionObjects), ["Add objects in radius to selection", "Add objects in radius around cursor to your selection."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = [true] call FUNC(getSelection);
        if (count _objects isNotEqualTo 0) then
        {
            addCuratorSelected (_objects);
            ["Added to selection"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_EQUALS, [false, true, true]]] call CBA_fnc_addKeybind; // Default: CTRL + ALT + =


[[COMPONENT_NAME, _CategoryName], QGVAR(RemoveSelectionObjects), ["Remove selected objects in radius from selection", "Remove selected objects in radius around the cursor from your selection."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = [true] call FUNC(getSelection);
        if (count _objects isNotEqualTo 0) then
        {
            setCuratorSelected ((curatorSelected select 0) - _objects);
            ["Removed selection"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_MINUS, [false, true, true]]] call CBA_fnc_addKeybind; // Default: CTRL + ALT + -(Not numpad one)


[[COMPONENT_NAME, _CategoryName], QGVAR(SelectAllObjects), ["Select all objects", "Adds all objects to current selection."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = curatorEditableObjects (getAssignedCuratorLogic player);
        if (count _objects isNotEqualTo 0) then
        {
            setCuratorSelected _objects;
            ["Selected all"] call FUNC(addKeybindUI);
        }
        else
        {
            ["", 12] call FUNC(addKeybindUI);
        };
        
        true; // Still consume input
    };
}, "", [DIK_A, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + A


[[COMPONENT_NAME, _CategoryName], QGVAR(FollowSelectedTarget), ["Follow selected", "Follows behind selected target, stops following when target has been deselected"],
{ 
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = (curatorSelected select 0);
        if (count _objects isNotEqualTo 0) then
        {
            // Use the first object.
            [_objects select 0] call FUNC(followTarget);
            ["Follow target"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_F, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + F


[[COMPONENT_NAME, _CategoryName], QGVAR(ForceDelete), ["Force delete marker or objects", "If map is open removes marker below cursor, otherwise force deletes selected objects"],
{
    if (call FUNC(canUseKeybind)) then
    {
        [] call FUNC(forceDelete);
        ["Force deleted"] call FUNC(addKeybindUI);
        true; // Still consume input
    };
}, ""] call CBA_fnc_addKeybind; // Default: Unbound


[[COMPONENT_NAME, _CategoryName], QGVAR(HideHints), ["Remove keybind UI and Hint", "Removes current hint(only from your screen) and hides the keybind UI"],
{
    if (call FUNC(canUseKeybind)) then
    {
        hintSilent "";
        [""] call FUNC(addKeybindUI);
        true; // Still consume input
    };
}, ""] call CBA_fnc_addKeybind; // Default: UNBOUND


[[COMPONENT_NAME, _CategoryName], QGVAR(TeleportToZeuses), ["Teleport to other Zeuses's locations", "Cycles through other Zeuses's locations."],
{
    if (call FUNC(canUseKeybind)) then
    {
        if (count allCurators > 1) then
        {
            [[] call FUNC(teleportToZeuses)] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 12] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_J, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + J


[[COMPONENT_NAME, _CategoryName], QGVAR(ToggleDamage), ["Toggle damage on selected", "Enables/disables damage on selected objects."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = (curatorSelected select 0);
        if (count _objects isNotEqualTo 0) then
        {
            // Use first object to determine the current state
            _damageable = isDamageAllowed (_objects select 0);
            private _newUIText = "Enabled damage";
            if (_damageable) then
            {
                _newUIText = "Disabled damage";
            };
            [_newUIText] call FUNC(addKeybindUI);

            {
                ["zen_common_allowDamage", [_x, !_damageable], _x] call CBA_fnc_targetEvent
            } forEach _objects;
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_D, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + D


[[COMPONENT_NAME, _CategoryName], QGVAR(ToggleLights), ["Toggle lights on selected objects or in radius", "Toggle lights on or off for selected objects or in radius around the cursor."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = [] call FUNC(getSelection);
        if (count _objects isNotEqualTo 0) then
        {
            [_objects] call FUNC(toggleLights);
            ["Toggled lights"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_L, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + L


[[COMPONENT_NAME, _CategoryName], QGVAR(ToggleSimulation), ["Toggle simulation on selected objects", "Turns simulation on/off on selected objects."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = (curatorSelected select 0);
        if (count _objects isNotEqualTo 0) then
        {
            // Use first object to determine the current state
            _simulated = simulationEnabled (_objects select 0);
            private _newUIText = "Set to simulated";
            if (_simulated) then
            {
                _newUIText = "Set to unsimulated";
            };
            [_newUIText] call FUNC(addKeybindUI);

            {
                ["zen_common_enableSimulationGlobal", [_x, !_simulated]] call CBA_fnc_serverEvent;
            } forEach _objects;
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, ""] call CBA_fnc_addKeybind; // Default: UNBOUND


[[COMPONENT_NAME, _CategoryName], QGVAR(ToggleVisibility), ["Toggle visibility on selected objects", "Makes selected objects invisible/visible."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _objects = (curatorSelected select 0);
        if (count _objects isNotEqualTo 0) then
        {
            // Use first object to determine the current state
            _hidden = isObjectHidden (_objects select 0);
            private _newUIText = "Set to hidden";
            if (_hidden) then
            {
                _newUIText = "Set to visible";
            };
            [_newUIText] call FUNC(addKeybindUI);

            {
                ["zen_common_hideObjectGlobal", [_x, !_hidden]] call CBA_fnc_serverEvent;
            } forEach _objects;
        }
        else
        { 
            ["", 0] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_N, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + N