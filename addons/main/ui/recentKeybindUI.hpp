class ctrlListNBox;
class RscText;

class RscTitles
{
    class Default 
    {
       idd = -1;
       fadein = 0;
       fadeout = 0;
       duration = 0;
    };

	class zeus_keybinds_main_recentKeybind_display
	{
	idd = -1;
	movingEnable = 1;
	fadein = 0.1;
	fadeout = 0.5;
	duration = 5;
	name = "zeus_keybinds_main_recentKeybind_display";
    onLoad = "uiNamespace setVariable ['zeus_keybinds_main_recentKeybind_display', _this select 0];";
	onUnLoad = "uinamespace setVariable ['zeus_keybinds_main_recentKeybind_display', nil]";
		class controls 
		{
			class Title: RscText
			{
				idc = IDC_RECENT_KEYBIND;
				text = "ERROR"; // this should be set when creating the UI
				style = ST_CENTER;
				x = "0.79 * safezoneW + safezoneX";
				y = "0.95 * safezoneH + safezoneY";
				w = "0.07 * safezoneW";
				h = "0.045 * safezoneH";
				colorBackground[] = {0,0,0,0.5};
			};
		};
	};
};