#include "\a3\ui_f\hpp\defineDIKCodes.inc"
private _CategoryName = "AI control";

[[COMPONENT_NAME, _CategoryName], QGVAR(GarrisonCursor), ["Garrison selected units near cursor", "Garrisons selected units near cursor's position(will use LAMBS garrison if possible)"], 
{
    if (call FUNC(canUseKeybind)) then
    {
        private _units = (curatorSelected select 0);
        _units = _units select { (_x isKindOf "CAManBase") && (alive _x) && !(isPlayer _x)};
        if (count _units isNotEqualTo 0) then
        {
            private _position = [] call zen_common_fnc_getPosFromScreen;
            private _radius = GVAR(effectValue);
            [_units, _position, _radius] call FUNC(garrisonUnits);
            ["Garrisoned units"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 1] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_O, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + O


[[COMPONENT_NAME, _CategoryName], QGVAR(UnGarrisonUnits), ["Ungarrison selected units", "Ungarrisons selected units"], 
{
    if (call FUNC(canUseKeybind)) then
    {
        private _units = (curatorSelected select 0);
        _units = _units select { (_x isKindOf "CAManBase") && (alive _x) && !(isPlayer _x)};
        if (count _units isNotEqualTo 0) then
        {
            [_units] call FUNC(unGarrisonUnits);
            ["Ungarrisoned units"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 1] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_O, [false, true, true]]] call CBA_fnc_addKeybind; // Default: CTRL + ALT + O


[[COMPONENT_NAME, _CategoryName], QGVAR(SuppressCursor), ["Suppress cursor's position", "Tell selected AI units to suppress the cursor's position"],
{
    if (call FUNC(canUseKeybind)) then
    {
        // get ATL world position
        private _position = ASLToATL ([] call zen_common_fnc_getPosFromScreen);
        private _units = (curatorSelected select 0);
        _units = _units select { (alive _x) && !(isPlayer _x)};
        if (count _units isNotEqualTo 0) then
        {
            {
                [_x, _position, GVAR(effectValue), 4, "MIDDLE"] call zen_ai_fnc_suppressiveFire;
            } forEach _units;
            ["Suppress cursor"] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 1] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_E, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + E


[[COMPONENT_NAME, _CategoryName], QGVAR(SwitchBehaviour), ["Switch behaviour", "Switches selected waypoints' and groups' behaviour between careless -> safe -> combat -> careless..."],
{
    if (call FUNC(canUseKeybind)) then
    {   
        // Find the groups from the selected units, in case only some units are selected in a group
        private _groups = [] call FUNC(uniqueGroups);
        if (count _groups isNotEqualTo 0) then
        {
            ([_groups, (curatorSelected select 2)] call FUNC(switchBehaviour)) call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 1] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_GRAVE, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + `


[[COMPONENT_NAME, _CategoryName], QGVAR(SwitchSpeed), ["Switch speed", "Switches selected waypoints' and groups' speed between full -> limited -> normal -> full..."],
{
    if (call FUNC(canUseKeybind)) then
    {
        // Find the groups from the selected units, in case only some units are selected in a group
        private _groups = [] call FUNC(uniqueGroups);
        if (count _groups isNotEqualTo 0) then
        {
            ([_groups, (curatorSelected select 2)] call FUNC(switchSpeed)) call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 1] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, ""] call CBA_fnc_addKeybind; // Default: Unbound


[[COMPONENT_NAME, _CategoryName], QGVAR(SwitchStance), ["Switch units' stance", "Switches selected units between standing -> kneeling -> proning -> standing..."],
{
    if (call FUNC(canUseKeybind)) then
    {
        private _units = (curatorSelected select 0);
        _units = _units select { (_x isKindOf "CAManBase") && (alive _x) && !(isPlayer _x)};
        if (count _units isNotEqualTo 0) then
        {
            ([_units] call FUNC(switchStance)) call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 1] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_S, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + S


[[COMPONENT_NAME, _CategoryName], QGVAR(ToggleAIPathing), ["Toggle AI pathing for selected", "Enable or disable pathing(stop AI from moving but not turning) for selected AI units"],
{ 
    if (call FUNC(canUseKeybind)) then
    {
        private _units = (curatorSelected select 0);
        if (count _units isNotEqualTo 0) then
        {
            private _enabledFeature = [_units, "PATH"] call FUNC(toggleAIFeature);
            private _newUIText = "Disabled pathing";
            if (_enabledFeature) then
            {
                _newUIText = "Enabled pathing";
            };
            [_newUIText] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 1] call FUNC(addKeybindUI); 
        };
        true; // Still consume input
    };
}, "", [DIK_M, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + M