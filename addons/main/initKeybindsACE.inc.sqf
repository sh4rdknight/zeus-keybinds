#include "\a3\ui_f\hpp\defineDIKCodes.inc"
private _ModComponent = COMPONENT_NAME + " ACE";

[_ModComponent, QGVAR(CycleCaptiveState), ["Cycles selected units' captive state", "Cycles selected units between surrendering -> handcuffed -> free -> surrenderring -> ..."], 
{
    if (call FUNC(canUseKeybind)) then
    {
        private _units = (curatorSelected select 0);
        _units = _units select { (_x isKindOf "CAManBase") && (alive _x) && !(isPlayer _x)};
        if (count _units isNotEqualTo 0) then
        {
            [[_units] call FUNC(ACE_switchCaptiveStatus)] call FUNC(addKeybindUI);
        }
        else
        { 
            ["", 1] call FUNC(addKeybindUI);
        };
        true; // Still consume input
    };
}, "", [DIK_APOSTROPHE, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + '


[_ModComponent, QGVAR(OpenMedicalMenu), ["Open unit's ACE medical menu", "Open selected unit's ACE medical menu"], 
{
    if (call FUNC(canUseKeybind)) then
    {
        private _units = (curatorSelected select 0);
        _units = _units select { (_x isKindOf "CAManBase") };
        if (count _units isNotEqualTo 0) then
        {
            _units = _units select 0;
            if ([player, _units] call ace_medical_gui_fnc_canOpenMenu) then
            {
                setCuratorSelected([_units]);
                [_units] call ace_medical_gui_fnc_openMenu;
            };
        }
        else
        {
            ["", 1] call FUNC(addKeybindUI);
        };
        true; // Still consume input
    };
}, "", [DIK_RBRACKET, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + ]


[_ModComponent, QGVAR(ToggleUnconscious), ["Toggle unconscious", "Toggles selected units between unconscious and conscious"], 
{
    if (call FUNC(canUseKeybind)) then
    {
        private _units = (curatorSelected select 0);
        _units = _units select { (_x isKindOf "CAManBase") && (alive _x) };
        if (count _units isNotEqualTo 0) then
        {
            private _newState = (_units select 0) getVariable ["ace_isunconscious", false];
            {
                if (_newState) then
                {
                    _x setVariable [QEGVAR(medical_statemachine,AIUnconsciousness), nil, true];
                }
                else
                {
                    _x setVariable [QEGVAR(medical_statemachine,AIUnconsciousness), true, true];
                };
                [_x, !_newState, 10e10] call ace_medical_fnc_setUnconscious;
            } forEach _units;
            [_units, 0.05] call FUNC(handleUnitsSideChanged);
            
            private _uiText = "Set to unconsious";
            if (_newState) then
            {
                _uiText = "Set to consious";
            };
            [_uiText] call FUNC(addKeybindUI);
        }
        else
        {
            ["", 1] call FUNC(addKeybindUI);
        };
        true; // Still consume input
    };
}, "", [DIK_SLASH, [false, true, false]]] call CBA_fnc_addKeybind; // Default: CTRL + /