## ABOUT:

**Zeus Keybinds** is a client side mod that adds more configurable keybinds for Zeus.

Requires [Zeus Enhanced(ZEN)](https://steamcommunity.com/sharedfiles/filedetails/?id=1779063631) to work. </br>
Additionally **supports ACE, LAMBS_Danger.fsm** mods. By default will use them for keybinds if possible, can be changed in settings.

The radius of garrison, heal area, the time AI suppresses cursor's position and similar are controller by "effect value" which can be changed by **"Increase/decrease effect value" keybinds.**

Options(disabling keybind UI and similar) can be changed in Options->Addon Options->Zeus keybinds.

Keybinds can be changed or removed in Options->Controls->Configure addons->Zeus keybinds.

## DEFAULT KEYBINDS:
* **Increase/decrease effect value:** Change the radius of garrison area, the time AI suppresses cursor's position, etc. Used for other keybinds. _Default keybind - **CTRL + ALT + PAGE UP/DOWN**._
* **Add objects in radius to editable list:** Add objects in radius around cursor to your editable list. _Default keybind - **CTRL + EQUALS**._
* **Remove selected objects or in radius from editable list:** Remove selected objects or in radius around the cursor from the your editable list. _Default keybind - **CTRL + MINUS(NOT KEYPAD ONE)**._
* **Add objects in radius to selection:** Add objects in radius around cursor to your selection. _Default keybind - **CTRL + ALT + EQUALS**._
* **Remove selected objects in radius from selection:** Remove selected objects in radius around the cursor from your selection. _Default keybind - **CTRL + ALT + MINUS(NOT KEYPAD ONE)**._
* **Clear inventory of selected:** Clears inventory of selected(excluding inventory of people). _Default keybind - **CTRL + I**._
* **Cycle door's state:** Cycles closest door to the cursor between being closed -> locked -> open -> closed... _Default keybind - **CTRL + B**._
* **Destroy buildings in radius:** Destroy buildings in radius around cursor. _Default keybind - **CTRL + END**._
* **Follow selected:** Follows behind selected target, stops following when target has been deselected. _Default keybind - **CTRL + F**._
* **Force delete marker or objects:** If map is open removes marker below cursor(even normally not deletable ones), otherwise will delete selected objects(even those that give "insufficient resources" error). _Default keybind - **UNBOUND**._
* **Garrison selected units near cursor:** Garrisons selected units near cursor's position(mod used is determined by preferred garrison setting). _Default keybind - **CTRL + O**._
* **Ungarrison selected units:** Ungarrisons selected units. _Default keybind - **CTRL + ALT + O**._
* **Heal selected:** Heal and repair selected objects. _Default keybind - **CTRL + H**._
* **Repair buildings in radius:** Repairs buildings in radius around the cursor. _Default keybind - **CTRL + ALT + H**._
* **Rearm selected or spawn an arsenal:** Will try to rearm selected, if nothing is selected will spawn an arsenal at cursor's position(Uses ZEN's "Preferred Arsenal" setting for type to spawn). _Default keybind - **CTRL + R**._
* **Remove keybind UI and Hint:** Removes current hint(only from your screen) and hides the keybind UI. _Default keybind - **UNBOUND**._
* **Remove ammo and drop units' weapons:** Removes vehicles' ammo and drops all weapons of selected units. _Default keybind - **CTRL + U**._
* **Select all objects:** Adds all objects to current selection. _Default keybind - **CTRL + A**._
* **Show ammo of selected:** Shows how much ammo of each type the selected has left. _Default keybind - **CTRL + [**._
* **Spawn a ghost unit at cursor and control it:** Spawns invincible, invisible unit and remote controls it, deleted after. _Default keybind - **CTRL + ;**._
* **Suppress cursor's position:** Tell selected AI units to suppress the cursor's position. _Default keybind - **CTRL + E**._
* **Switch behaviour:** Switches selected waypoints' and groups' behaviour between careless -> safe -> combat -> careless... _Default keybind - **CTRL + `**._
* **Switch speed:** Switches selected waypoints' and groups' speed between full -> limited -> normal -> full... _Default keybind - **UNBOUND**._
* **Switch units' stance:** Switches selected units between standing -> kneeling -> proning -> standing... _Default keybind - **CTRL + S**._
* **Teleport to other Zeuses's locations:** Cycles through other Zeuses's locations. _Default keybind - **CTRL + J**._
* **Toggle AI pathing for selected:** Enable or disable pathing(stops AI from moving but not turning) for selected AI units. _Default keybind - **CTRL + M**._
* **Toggle damage on selected:** Turns damage on/off on selected objects. _Default keybind - **CTRL + D**._
* **Toggle lights on selected or in radius:** Toggle lights on or off for selected objects or in radius around the cursor. _Default keybind - **CTRL + L**._
* **Toggle simulation on selected:** Turns simulation on/off on selected objects. _Default keybind - **UNBOUND**._
* **Toggle visibility on selected:** Makes selected objects invisible/visible. _Default keybind - **CTRL + N**._

## ACE KEYBINDS:
Requires ACE to be loaded.
* **Cycles selected units' captive state:** Cycles selected units between surrendering -> handcuffed -> free -> surrenderring... _Default keybind - **CTRL + '**._
* **Open unit's ACE medical menu:** Open selected unit's ACE medical menu. _Default keybind - **CTRL + ]**._
* **Toggle unconscious:** Toggles selected units between unconscious and conscious. _Default keybind - **CTRL + /**._

## CREDITS:
Mod by [sh4rdknight.](https://gitlab.com/sh4rdknight)<br/>
Source code available at [Gitlab.](https://gitlab.com/sh4rdknight/zeus-keybinds)

Huge thanks to [johnb43](https://github.com/johnb432) for help while making the mod.

## LICENSE:
[Zeus Keybinds is licensed under the GNU General Public License (GPLv3).](https://gitlab.com/sh4rdknight/zeus-keybinds/-/blob/main/LICENSE)

## HOW TO BUILD THIS PROJECT:
[HEMTT documentation](https://brettmayson.github.io/HEMTT/installation.html)
* Download HEMTT from [here.](https://github.com/BrettMayson/HEMTT/releases)
* Unpack the downloaded file inside the project's directory.
* Open CMD or Command line inside the project's directory.
* Type in "hemtt release" to get a ZIP file in /releases directory.
* Unzip and add the mod through the Arma launcher.